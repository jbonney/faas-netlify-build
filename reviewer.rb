# frozen_string_literal: true

require 'netlify'
require 'git'
require 'front_matter_parser'
require 'faraday'
require 'fileutils'

module FaasNetlifyBuild
  # Reviewer class is responsible for connecting to Bitbucket and pull the code, check whether a new build is
  # needed or not, and if so trigger the build to Netlify
  class Reviewer
    # Constructor
    #
    # @params args [Hash] the secrets from Bitbucket and Netlify (:bitbucket_token, :netlify_token,
    # :netlify_site_id, :netlify_build_hook)
    def initialize(args)
      @bitbucket_token = args[:bitbucket_token]
      @netlify_token = args[:netlify_token]
      @netlify_site_id = args[:netlify_site_id]
      @netlify_build_hook = args[:netlify_build_hook]
      @tmp_location = args[:config]['TMP_FOLDER']
    end

    # Logic to be executed to validate if a new build is required
    def run
      output = 'No new build was needed...'
      checkout_code
      if new_build_needed?
        trigger_build
        output = 'New build was triggered'
      end
      output
    end

    # Clone the git repository in a local folder
    def checkout_code
      p 'Cloning repository...'
      # Delete the directory if it already exists
      FileUtils.rm_rf(@tmp_location)
      # Clone the repo in the tmp folder
      Git.clone("https://jbonney:#{@bitbucket_token}@bitbucket.org/jbonney/jimmybonney.com.git", @tmp_location)
    end

    # Check if a new build is needed. A new build is needed if:
    # 1. There is at least one article that has a publication date set to today or in the past and
    # 2. The last deployment date from Netlify is older than the article publication date
    #
    # @return [Boolean] true if a new build is needed, false otherwise
    def new_build_needed?
      p 'Validating if a new build is needed...'
      last_deployment = last_deployment_date
      current_year = Date.today.year
      new_build_needed = false
      # Initiate the loader for front_matter_parser to allow dates
      loader = FrontMatterParser::Loader::Yaml.new(allowlist_classes: [Date])
      # Articles are sorted per year, in the folder 'content/articles/[YEAR]'
      Dir.glob("#{@tmp_location}/content/articles/#{current_year}/*.md") do |file|
        parsed = FrontMatterParser::Parser.parse_file(file, loader: loader)
        p "Analyzing article #{parsed['title']}"
        # If article explicitly set the publish flag to false, then don't do anything, move on to the next file
        next if parsed['publish'] == false

        # New build is needed if:
        # 1. the article created_at date is in the past AND
        # 2. the last deployment took place before the article created_at date
        new_build_needed = true if parsed['created_at'] > last_deployment && parsed['created_at'] <= Date.today
      end
      p "New build needed: #{new_build_needed}"
      new_build_needed
    end

    # Trigger the webhook from Netlify to start a new build
    def trigger_build
      p 'Triggering the build...'
      response = Faraday.post(@netlify_build_hook) do |req|
        req.params['trigger_title'] = 'Triggered automatically from Ruby script using webhook'
      end
      p "Build hook post request status: #{response.status}"
      # TODO: handle errors in the response
    end

    # Fetch the last date at which a deployment took place
    #
    # @return [Date] last deployment date
    def last_deployment_date
      p 'Checking last deployment date...'
      # Connect to Netlify
      netlify_client = Netlify::Client.new(access_token: @netlify_token)
      # Get the correct site (i.e. jimmybonney.com)
      site = netlify_client.sites.get(@netlify_site_id)
      # The Netlify API sort deploys from most recent to oldest => first element is the most recent deployment
      last_deploy = site.deploys.all.first
      last_deploy_timestamp = last_deploy.attributes[:created_at]
      DateTime.parse(last_deploy_timestamp).to_date
    end
  end
end
