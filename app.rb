# frozen_string_literal: true

require 'functions_framework'
require 'google/cloud/secret_manager'
require 'yaml'
require './reviewer'

# Use Google Cloud Secret Manager client to decode a secret
#
# @param client [Google::Cloud::SecretManager] the Google Cloud Secret Manager client
# @param secret_name [String] full path of secret to decode]
def extract_secret(client, secret_name)
  secret = client.access_secret_version name: secret_name
  secret.payload.data
end

# Fetch the secret, either from the file system if the secret is mounted as a file,
# or through a secret manager client otherwise
#
# @param secret_file [String] the path to the secret when it is mounted
# @param secret_manager_client [Google::Cloud::SecretManager] secret manager client
# @param secret_arn [String] the full secret identifier, including project number, name and version
# @return [String] the secret
def fetch_secret(secret_file, secret_manager_client, secret_arn)
  return File.read(secret_file) if File.exist?(secret_file)

  extract_secret(secret_manager_client, secret_arn)
end

# Load configuration file .env.yaml
config = YAML.load_file(File.join(__dir__, '.env.yml'))

# Entry point for the Cloud Function
#
# Read the relevant secrets and then initiate and run a Reviewer instance
FunctionsFramework.http 'build_scheduler' do |_request|
  # In the local environment, the json key should be present to allow us to read secrets from Secret Manager
  # This file is not commited (part of .gitignore, and therefore not sent to GCP either)
  if File.exist?('.credentials/secret_manager_secret_accessor.json')
    # Read the key only if it is not set from the environment already.
    ENV['GOOGLE_APPLICATION_CREDENTIALS'] ||= '.credentials/secret_manager_secret_accessor.json'
  end
  # Initiate the Secret Manager client, either by using the credentials defined above when this is
  # run locally, or the proper credentials will be looked up internally by GCP when this codes run on GCP
  client = Google::Cloud::SecretManager.secret_manager_service

  # Project ID is either read from the environment variable (usually when running inside GCP),
  # or through the config file
  project_id = ENV['GCP_PROJECT'] || config['PROJECT_ID']

  # Fetch the different secrets needed
  bitbucket_token = fetch_secret(
    '/secrets/BITBUCKET_TOKEN', client, "projects/#{project_id}/secrets/BITBUCKET_TOKEN/versions/latest"
  )
  netlify_token = fetch_secret(
    '/secrets/NETLIFY_TOKEN', client, "projects/#{project_id}/secrets/NETLIFY_TOKEN/versions/latest"
  )
  netlify_site_id = fetch_secret(
    '/secrets/NETLIFY_SITE_ID', client, "projects/#{project_id}/secrets/NETLIFY_SITE_ID/versions/latest"
  )
  netlify_build_hook = fetch_secret(
    '/secrets/NETLIFY_BUILD_HOOK', client, "projects/#{project_id}/secrets/NETLIFY_BUILD_HOOK/versions/latest"
  )

  reviewer = FaasNetlifyBuild::Reviewer.new(
    bitbucket_token: bitbucket_token,
    netlify_token: netlify_token,
    netlify_site_id: netlify_site_id,
    netlify_build_hook: netlify_build_hook,
    config: config
  )
  reviewer.run
end
